import express from 'express';
import userRoute from './user/user.route';
import CategoryRoute  from './category/category.route';
import ProductRoute  from './product/product.route';
import OrderRoute  from './order/order.route';
import FavouriteRoute  from './favourite/favourite.route';
import CommentRoute  from './comment/comment.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AdminRoute  from './admin/admin.route';
import slidersRoute  from './slider/slider.route';

import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);
router.use('/categories',CategoryRoute);
router.use('/products', ProductRoute);
router.use('/orders',OrderRoute);
router.use('/favourites',FavouriteRoute);
router.use('/comments',CommentRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/sliders',slidersRoute);
router.use('/admin',requireAuth, AdminRoute);
export default router;
