import express from 'express';
import CommentController from '../../controllers/comment/comment.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:productId/products')
    .post(
        requireAuth,
        CommentController.validateBody(),
        CommentController.create
    )
    
router.route('/')
    .get(CommentController.findAll);
    
router.route('/:commentId')
    .put(
        requireAuth,
        CommentController.validateBody(true),
        CommentController.update
    )
    .get(CommentController.findById)
    .delete( requireAuth,CommentController.delete);







export default router;