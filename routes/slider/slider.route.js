import express from 'express';
import SliderController from '../../controllers/slider/slider.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        multerSaveTo('slider').single('image'),
        SliderController.create
    )
    .get(SliderController.findAll);

router.route('/pagination')
    .get(SliderController.findAllPaginated);

router.route('/:sliderId')
    .delete(SliderController.delete);

export default router;