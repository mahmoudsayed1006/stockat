import express from 'express';
import OrderController from '../../controllers/order/order.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:userId/users')
    .post(
        requireAuth,
        OrderController.validateCreatedOrders(),
        OrderController.create
    );

router.route('/')
    .get(OrderController.findOrders)

router.route('/:orderId')
    .get(OrderController.findById)
    .delete( requireAuth,OrderController.delete);
router.route('/:orderId/users/:userId/status')
    .put( requireAuth,OrderController.validateThenUpdateOrderStatus())



export default router;
