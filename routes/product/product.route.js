import express from 'express';
import ProductController from '../../controllers/product/product.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('products').array('img',100),
        ProductController.validateCreatedProduct(),
        ProductController.create
    );

router.route('/')
    .get(ProductController.findAll);

router.route('/:productId')
    .get(ProductController.findById)
    .put(
        requireAuth,
        multerSaveTo('products').array('img', 100),
        ProductController.validateCreatedProduct(true),
        ProductController.update
    )
    .delete(ProductController.delete);

router.route('/:productId/mainImg')
    .put(
        requireAuth,
        ProductController.updateMainImg
    )
router.route('/:productId/active')
    .put(
        requireAuth,
        ProductController.active
    )

router.route('/:productId/dis-active')
    .put(
        requireAuth,
        ProductController.disactive
    )

router.route('/:productId/sponsered')
    .put(
        requireAuth,
        ProductController.sponsered
    )

router.route('/:productId/unsponsered')
    .put(
        requireAuth,
        ProductController.unsponsered
    )


export default router;
