import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';


const SliderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    image: {
        type: String,
        required: true,
        trim: true
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

SliderSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});


SliderSchema.plugin(autoIncrement.plugin, { model: 'slider', startAt: 1 });

export default mongoose.model('slider', SliderSchema);
