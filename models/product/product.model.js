import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const ProductSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
    },
    quantity:{
        type:Number,
        required:true
    },
    category: {
        type: [Number],
        ref: 'category'
    },
    img: [{
        type: String,
        required: true,
        
    }],
    description:{
        type:String,
        required:true
    },
    specifications:{
        type:[String],
        required:true
    },
    owner:{
        type:Number,
        ref:'user',
        required:true
    },
    visible: {
        type: Boolean,
        default: false
    },
    advertisingType: {
        type: String,
        enum: ['price', 'auction'],
        default: 'auction'
    },
    sponsered: {
        type: Boolean,
        default: false
    },
    comments:{
        type:[Number]
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
ProductSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
ProductSchema.plugin(autoIncrement.plugin, { model: 'product', startAt: 1 });

export default mongoose.model('product', ProductSchema);