import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Product from "../../models/product/product.model";
import Favourite from "../../models/favourite/favourite.model";
import Notif from "../../models/notif/notif.model";
import ApiError from '../../helpers/ApiError';
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    { path: 'product', model: 'product' },
    { path: 'user', model: 'user' }
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { userId } = req.params;
            let query = { user: userId,deleted:false };
            let favourites = await Favourite.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const favouritesCount = await Favourite.count(query);
            const pageCount = Math.ceil(favouritesCount / limit);

            res.send(new ApiResponse(favourites, page, pageCount, limit, favouritesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async create(req, res, next) {
        try {
            let {productId,userId } = req.params;
            let user = await checkExistThenGet(req.user._id, User);
            await checkExist(productId, Product);
            let favourite =  await Favourite.create({ user: req.user._id, product: productId });
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: favourite.id,
                subjectType: user.firstname +' '+ user.lastname + ' favourite your add'
            });
            let notif = {
                "description":user.firstname +' '+ user.lastname + ' favourite your add'
            }
            await Notif.create({...notif,resource:req.user,target:userId,subject:favourite.id});
            res.status(201).send();
        } catch (error) {
            next(error)
        }
    },
    async delete(req, res, next) {
        try {
            let {favouriteId } = req.params;
            let favourite = await checkExistThenGet(favouriteId, Favourite, { deleted: false });
            if (favourite.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));

            favourite.deleted = true;
            await favourite.save();
            res.status(201).send();
        } catch (error) {
            next(error)
        }
    },

}