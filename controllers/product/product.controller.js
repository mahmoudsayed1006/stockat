import Product from "../../models/product/product.model";
import ApiResponse from "../../helpers/ApiResponse";
import Category from "../../models/category/category.model";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { checkExistThenGet, checkExist, isArray } from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import ApiError from '../../helpers/ApiError';
import Comments from "../../models/comment/comment.model";
import Favourite from "../../models/favourite/favourite.model";

const populateQuery = [
    { path: 'category', model: 'category' },
    { path: 'owner', model: 'user' },
    { path: 'comments', model: 'comment' }
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {categoryId, ownerId ,sponsered,visible,type} = req.query;
            let query = {deleted: false };
            if (categoryId) query.category = categoryId;
            if (ownerId) query.owner = ownerId;
            if (sponsered) query.sponsered = sponsered;
            if (type) query.advertisingType = type;
            if (visible) query.visible = visible;
            let products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const productsCount = await Product.count(query);
            const pageCount = Math.ceil(productsCount / limit);
            res.send(new ApiResponse(products, page, pageCount, limit, productsCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedProduct(isUpdate = false) {
        
        let validations = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('description').not().isEmpty().withMessage('description is required'),
            body('specifications').not().isEmpty().withMessage('specifications is required'),
            body('price'),
            body('quantity').not().isEmpty().withMessage('quantity is required'),
            body('advertisingType').not().isEmpty().withMessage('advertisingType is required')
            .isIn(['auction','price']).withMessage('wrong type'),
            body('category').not().isEmpty().withMessage('category is required')
            .isNumeric().withMessage('numeric value required'),
            
        ];
    
        return validations;
    },
    async create(req, res, next) {
        try {
            let userId = req.user._id;
           
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.category, Category, { deleted: false }),
            checkExist(userId, User, { deleted: false });
            let img = await handleImgs(req);
            console.log(req.file);
            console.log(img);
            let product = await Product.create({
                ...validatedBody,
                img: img, 
                owner: userId
            });
            let reports = {
                "action":"Create New Product",
            };
            let report = await Report.create({...reports, user: req.user });
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: product.id,
                    subjectType: 'user add new product'
                });
                let notif = {
                    "description":'new product'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:product.id});
            });
            res.status(201).send(product);
            
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { productId } = req.params;

            await checkExist(productId, Product,
                { deleted: false });
            let product = await Product.findById(productId).populate(populateQuery);
            res.send(
                product
            );
        } catch (err) {
            next(err);
        }
    },

    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.visible = true;
            await product.save();
            let reports = {
                "action":"Active Product",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: product.id,
                subjectType: 'stockat accept your Product'
            });
            let notif = {
                "description":'stockat accept your Product'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:product.id});
            res.send('product active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            product.visible = false;
            await product.save();
            let reports = {
                "action":"Dis-active Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product dis-active');
        } catch (error) {
            next(error);
        }
    },
    
    async sponsered(req, res, next) {
        try {
            let user = req.user;
            
            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            if (user.type != 'ADMIN' || product.owner != req.user._id)
                return next(new ApiError(403, ('not allowes')));

            product.sponsered = true;
            await product.save();
            let reports = {
                "action":"Make Product sponsered",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: product.id,
                subjectType: 'stockat Make your Product sponsered'
            });
            let notif = {
                "description":'stockat Make your Product sponsered'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:product.id});
            res.send('product been sponsered');
           
        } catch (error) {
            next(error);
        }
    },

    async unsponsered(req, res, next) {
        try {
            let user = req.user;
           
            let { productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            if (user.type != 'ADMIN' || product.owner != req.user._id)
                return next(new ApiError(403, ('not allowes')));

            product.sponsered = false;
            await product.save();
            let reports = {
                "action":"Make Product unsponsered",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: product.id,
                subjectType: 'stockat Make your Product unsponsered'
            });
            let notif = {
                "description":'stockat Make your Product unsponsered'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:product.id});
            res.send('product been unsponsered');
 
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            
            let {productId } = req.params;
            await checkExist(productId, Product,
                {deleted: false });

            const validatedBody = checkValidations(req);
            let img = await handleImgs(req);
            let updatedProduct = await Product.findByIdAndUpdate(productId, {
                ...validatedBody,
                img:img

            }, { new: true }).populate(populateQuery);

            let reports = {
                "action":"Update Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedProduct);
        }
        catch (err) {
            next(err);
        }
    },
    async updateMainImg(req, res, next) {
        try {
            
            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product, { deleted: false });
            let data = [];
            data = product.img;
            console.log(data);
            let index = req.body.index;
            data.unshift(data.splice(index, 1)[0]);
            product.img = data;
            await product.save();
            res.status(200).send(product);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let {productId } = req.params;

            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            let comments = await Comments.find({ product: productId });
            for (let comment of comments ) {
                comment.deleted = true;
                await comment.save();
            }
            let favourites = await Favourite.find({ product: productId });
            for (let favourite of favourites ) {
                favourite.deleted = true;
                await favourite.save();
            }
            product.deleted = true
            await product.save();
            let reports = {
                "action":"Delete Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    }
}