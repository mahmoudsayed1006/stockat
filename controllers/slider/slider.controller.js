import Slider from "../../models/slider/slider.model";
import { handleImg } from "../shared/shared.controller";
import ApiResponse from "../../helpers/ApiResponse";
import { checkExistThenGet } from "../../helpers/CheckMethods";

export default{
    async findAll(req, res, next) {
        try {
            let query = { deleted: false };
            let sliders = await Slider.find(query)
                .sort({ createdAt: -1 });
            res.send(sliders)
        } catch (err) {
            next(err);
        }
    },
    
    async findAllPaginated(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;

            let query = { deleted: false };
            let sliders = await Slider.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const slidersCount = await Slider.count(query);
            const pageCount = Math.ceil(slidersCount / limit);

            res.send(new ApiResponse(sliders, page, pageCount, limit, slidersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async create(req, res, next) {
        try {
            let image = await handleImg(req);

            let createdSlider = await Slider.create({ image: image });
            res.status(201).send(createdSlider);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { sliderId } = req.params;

            let slider = await checkExistThenGet(sliderId, Slider, { deleted: false });
            slider.deleted = true;

            await slider.save();

            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },
}