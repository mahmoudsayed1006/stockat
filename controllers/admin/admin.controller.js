import ApiError from "../../helpers/ApiError";
import Category from "../../models/category/category.model";
import User from "../../models/user/user.model";
import Order from "../../models/order/order.model";
import Product from "../../models/product/product.model";
import Report from "../../models/reports/report.model";
const populateQuery = [
    { path: 'client', model: 'user' },
    {
        path: 'productOrders.product', model: 'product',
        populate: { path: 'category', model: 'category' }
    }
];
const action = [
    { path: 'user', model: 'user' },
]
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'CLIENT'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

    async getLastOrder(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let { status} = req.query
            let query = {deleted: false };
            if (status)
                query.status = status;                
            let lastOrder = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastOrder);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const usersCount = await User.count(query);
            const categoryCount = await Category.count(query);
            const PendingOrdersCount = await Order.count({deleted:false,status:'PENDING'});
            const AcceptedOrdersCount = await Order.count({deleted:false,status:'ACCEPTED'});
            const RefusedOrdersCount = await Order.count({deleted:false,status:'REFUSED'});
            const DeliveredOrdersCount = await Order.count({deleted:false,status:'DELIVERED'});
            const OnWayOrdersCount = await Order.count({deleted:false,status:'ON_THE_WAY'});
            const productsCount = await Product.count(query);

            let total = await Order.find(query).distinct('total');
            var sum = 0;
            for (var i = 0; i < total.length; i++) { 
              sum += total[i]
            }
            
            res.status(200).send({
                users:usersCount,
                category:categoryCount,
                pending:PendingOrdersCount,
                accepted:AcceptedOrdersCount,
                refused:RefusedOrdersCount,
                delivered:DeliveredOrdersCount,
                onWay:OnWayOrdersCount,
                products:productsCount,
                totalSales:sum
                
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}