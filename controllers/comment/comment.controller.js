import ApiResponse from "../../helpers/ApiResponse";
import ApiError from '../../helpers/ApiError';
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Comments from "../../models/comment/comment.model";
import Product from "../../models/product/product.model";
const populateQuery = [
    { path: 'product', model: 'product' },
    { path: 'user', model: 'user' }
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {productId, userId} = req.query;
            let query = {deleted: false };
            if (productId) query.product = productId;
            if (userId) query.user = userId;
            let comments = await Comments.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const commentsCount = await Comments.count(query);
            const pageCount = Math.ceil(commentsCount / limit);

            res.send(new ApiResponse(comments, page, pageCount, limit, commentsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('comment').not().isEmpty().withMessage('comment is required'),
            body('price').not().isEmpty().withMessage('price is required')
                .isFloat({ min: 1 }).withMessage('price should be greater than 1'),

        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            let {productId} = req.params;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let createdComment = await Comments.create({ ...validatedBody,user:user,product:productId});
            let product = await checkExistThenGet(productId, Product);
            product.comments.push(createdComment._id);
            await product.save();
            let reports = {
                "action":"Create Comment",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: product.owner, 
                fromUser: req.user, 
                text: 'new notification',
                subject: createdComment.id,
                subjectType: user.firstname +' '+ user.lastname + ' add comment on your add'
            });
            let notif = {
                "description":user.firstname +' '+ user.lastname + ' add comment on your add'
            }
            await Notif.create({...notif,resource:req.user,target:product.owner,subject:createdComment.id});
            res.status(201).send(createdComment);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { commentId } = req.params;
            await checkExist(commentId, Comments, { deleted: false });
            let comment = await Comments.findById(commentId).populate(populateQuery);
            res.send(comment);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            let { commentId } = req.params;
            let comment = await checkExistThenGet(commentId, Comments, { deleted: false });
            if (comment.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));

            const validatedBody = checkValidations(req);
            let updatedcomment = await Comments.findByIdAndUpdate(commentId, {
                ...validatedBody,
            }, { new: true });
            
            let reports = {
                "action":"Update Comment",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedcomment);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            let { commentId } = req.params;
            let comment = await checkExistThenGet(commentId, Comments, { deleted: false });
            if (user.type != 'ADMIN' || comment.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));
            comment.deleted = true;
            await comment.save();
            let reports = {
                "action":"Delete Comment",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};